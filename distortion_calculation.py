import numpy
from scipy.linalg import polar, expm
import os, fnmatch
import pickle
import matplotlib.pyplot as plt
import scipy.constants as const
import timeit
from functools import reduce

class Unitary_Matrix_Parameterization():

    def __init__(self, unitary_matrix=numpy.zeros((0,0)), parameter_vector=numpy.zeros(0), matrix_shape=(0,0)):
        self.matrix_decomposition=None
        self.matrix_shape=matrix_shape
        if(numpy.all(unitary_matrix.shape!=(0,0))):
            self.set_matrix(unitary_matrix)
        if(numpy.all(parameter_vector.size!=0)):
            self.set_parameter_vector(parameter_vector, matrix_shape)

    def set_matrix(self, input_matrix):
        self.unitary_matrix=input_matrix
        self.parameter_vector=numpy.zeros(0)
        self.matrix_shape=input_matrix.shape

    def set_parameter_vector(self, parameter_vector, matrix_shape):
        self.matrix_shape=matrix_shape
        self.parameter_vector=parameter_vector
        self.unitary_matrix=numpy.zeros((0,0))
        
        
    def get_parameter_vector(self):
        unitary_matrix=self.unitary_matrix
        n_row=unitary_matrix.shape[0]
        n_column=unitary_matrix.shape[1]
        parameter_vector=numpy.zeros(0)
        matrix_decomposition=numpy.zeros((0, n_row, n_row), dtype=complex)
        rotation_matrix=lambda theta:numpy.array([[numpy.sin(theta), numpy.cos(theta)],[-numpy.cos(theta),numpy.sin(theta)]])
        for column_index in range(n_column):
            column_vector=unitary_matrix[column_index:,column_index]
            column_angles=numpy.angle(column_vector)
            parameter_vector=numpy.append(parameter_vector, column_angles)
            D=numpy.ones(n_row, dtype=complex)            
            D[column_index:]=numpy.exp(-1.0j*column_angles)
            D=numpy.diag(D)
            unitary_matrix=(D).dot(unitary_matrix)
            D=(D).T.conj()
            matrix_decomposition=numpy.append(matrix_decomposition, D[numpy.newaxis], axis=0)
            for row_index in range(n_row-2,column_index-1,-1):
                a=numpy.real(unitary_matrix[row_index][column_index])
                b=numpy.real(unitary_matrix[row_index+1][column_index])
                theta=numpy.arctan2(a,b)
                parameter_vector=numpy.append(parameter_vector, [theta])
                rotation_mat=rotation_matrix(theta)
                given_matrix=numpy.eye(n_row)
                given_matrix[row_index:(row_index+2),row_index:(row_index+2)]=rotation_mat
                unitary_matrix=given_matrix.dot(unitary_matrix)
                given_matrix=given_matrix.T.conj()
                matrix_decomposition=numpy.append(matrix_decomposition, given_matrix[numpy.newaxis], axis=0)
        self.matrix_decomposition=matrix_decomposition
        self.parameter_vector=parameter_vector
        return parameter_vector

    def get_unitary_matrix(self):
        parameter_vector=self.parameter_vector
        n_row=self.matrix_shape[0]
        n_column=self.matrix_shape[1]
        matrix_decomposition=numpy.zeros((0, n_row, n_row), dtype=complex)
        rotation_matrix=lambda theta:numpy.array([[numpy.sin(theta), numpy.cos(theta)],[-numpy.cos(theta),numpy.sin(theta)]])
        vector_index=0
        for column_index in range(n_column):
            column_angles=parameter_vector[vector_index:vector_index+(n_row-column_index)]
            vector_index=vector_index+(n_row-column_index)
            D=numpy.ones(n_row, dtype=complex)
            D[column_index:]=numpy.exp(-1.0j*column_angles)
            D=numpy.diag(D).T.conj()
            matrix_decomposition=numpy.append(matrix_decomposition, D[numpy.newaxis], axis=0)
            for row_index in range(n_row-2,column_index-1,-1):
                theta=parameter_vector[vector_index]
                vector_index=vector_index+1
                rotation_mat=rotation_matrix(theta)
                given_matrix=numpy.eye(n_row)
                given_matrix[row_index:(row_index+2),row_index:(row_index+2)]=rotation_mat
                given_matrix=given_matrix.T.conj()
                matrix_decomposition=numpy.append(matrix_decomposition, given_matrix[numpy.newaxis], axis=0)
        self.matrix_decomposition=matrix_decomposition
        unitary_matrix=reduce(numpy.dot, matrix_decomposition)
        self.unitary_matrix=unitary_matrix
        return unitary_matrix
    
class LBG_scalar_Quantizer():

    def __init__(self, code_books):
        self.code_books=code_books
    
    def get_quantized_vector(self, input_matrix):
        input_matrix_shape=input_matrix.shape[0]
        U=polar(input_matrix)[0]
        parameterizer=Unitary_Matrix_Parameterization()
        parameterizer.set_matrix(U)
        parameter_vector=parameterizer.get_parameter_vector()
        code_books=self.code_books
        quantized_vector=[]
        for i in range(code_books.shape[0]):
            codebook=code_books[i]
            diff_values=numpy.abs(codebook-parameter_vector[i])**2
            quantized_vector.append(codebook[numpy.argmin(diff_values)])

        quantized_vector=numpy.array(quantized_vector)
        parameterizer.set_parameter_vector(quantized_vector, input_matrix.shape)
        quantized_matrix=parameterizer.get_unitary_matrix()
        return quantized_matrix

    def get_distortion(self, U):
        input_matrix_shape=U.shape[0]
        parameterizer=Unitary_Matrix_Parameterization()
        parameterizer.set_matrix(U)
        parameter_vector=parameterizer.get_parameter_vector()
        code_books=self.code_books
        quantized_vector=[]
        for i in range(code_books.shape[0]):
            codebook=code_books[i]
            diff_values=numpy.abs(codebook-parameter_vector[i])**2
            quantized_vector.append(codebook[numpy.argmin(diff_values)])

        quantized_vector=numpy.array(quantized_vector)
        parameterizer.set_parameter_vector(quantized_vector, U.shape)
        quantized_matrix=parameterizer.get_unitary_matrix()
        distortion=numpy.linalg.norm(U-quantized_matrix, 'fro')
        return distortion

    

total_PM_matrix=[]
kappa_values=[1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 40.0, 60.0, 80.0, 100.0]
for kappa_value in kappa_values:
    directory_name='./../FMF_data/kappa_'+str(int(kappa_value))
    file_names=os.listdir(directory_name)
    PM_matrices=numpy.zeros((0,6,6))
    for file_name in file_names[0:40]:
        relative_file_name=directory_name+'/'+file_name
        data=numpy.load(relative_file_name)
        F_list=data[1]
        PM_delays, PMs=numpy.linalg.eig(F_list)
        PM_matrices=numpy.append(PM_matrices, PMs, axis=0)
    total_PM_matrix.append(PM_matrices)
total_PM_matrix=numpy.array(total_PM_matrix)

model_2=[]
model_3=[]
model_4=[]
GUE_2=[]
GUE_3=[]
GUE_4=[]
for n_bits in [2,3,4]:
    for kappa_index in range(len(kappa_values)):
        kappa_value=kappa_values[kappa_index]
        sample_PMs=total_PM_matrix[kappa_index]
        
        file_name='./kappa_'+str(int(kappa_value))+'_'+str(n_bits)+'bits.npy'
        codebook_model=numpy.load(file_name)        
        quantizer=LBG_scalar_Quantizer(codebook_model)
        distortion_list=list(map(quantizer.get_distortion, sample_PMs))
        avg_distortion=numpy.average(distortion_list)
        exec('model_'+str(n_bits)+'.append(avg_distortion)')

        file_GUE='./GUE_'+str(n_bits)+'bits.npy'
        codebook_GUE=numpy.load(file_GUE)
        quantizer_GUE=LBG_scalar_Quantizer(codebook_GUE)
        distortion_list_GUE=list(map(quantizer_GUE.get_distortion, sample_PMs))
        avg_distortion_GUE=numpy.average(distortion_list_GUE)
        exec('GUE_'+str(n_bits)+'.append(avg_distortion_GUE)')
plt.plot(kappa_values,GUE_2,'rD-',label='GUE codebook (2bits/parameter)',markevery=2, markeredgecolor='black',markeredgewidth=0.8)
plt.plot(kappa_values,model_2,'g^-',label='FMF model codebook (2bits/parameter)',markevery=2, markeredgecolor='black',markeredgewidth=0.8)
plt.plot(kappa_values,GUE_3,'bo-',label='GUE codebook (3bits/parameter)',markevery=2, markeredgecolor='black',markeredgewidth=0.8)
plt.plot(kappa_values,model_3,'kh-', label='FMF model codebook (3bits/parameter)',markevery=2, markeredgecolor='black',markeredgewidth=0.8)
plt.plot(kappa_values,GUE_4,'yx-', label='GUE codebook (4bits/parameter)',markevery=2, markeredgecolor='black',markeredgewidth=0.8)
plt.plot(kappa_values,model_4,'cs-', label='FMF model codebook (4bits/parameter)',markevery=2, markeredgecolor='black',markeredgewidth=0.8)
plt.xlabel(r'$\sigma^2_{\kappa}$ (in $m^{-2}$)')
plt.ylabel('average distortion')
plt.gca().set_ylim([0.0,3.5])
plt.legend()
plt.savefig('distortion')
plt.close()
