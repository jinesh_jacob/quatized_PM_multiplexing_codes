import numpy
from scipy.linalg import polar, expm
import os, fnmatch
import pickle
import matplotlib.pyplot as plt
import scipy.constants as const
import timeit
from functools import reduce


class Unitary_Matrix_Parameterization():

    def __init__(self, unitary_matrix=numpy.zeros((0,0)), parameter_vector=numpy.zeros(0), matrix_shape=(0,0)):
        self.matrix_decomposition=None
        self.matrix_shape=matrix_shape
        if(numpy.all(unitary_matrix.shape!=(0,0))):
            self.set_matrix(unitary_matrix)
        if(numpy.all(parameter_vector.size!=0)):
            self.set_parameter_vector(parameter_vector, matrix_shape)

    def set_matrix(self, input_matrix):
        self.unitary_matrix=input_matrix
        self.parameter_vector=numpy.zeros(0)
        self.matrix_shape=input_matrix.shape

    def set_parameter_vector(self, parameter_vector, matrix_shape):
        self.matrix_shape=matrix_shape
        self.parameter_vector=parameter_vector
        self.unitary_matrix=numpy.zeros((0,0))
        
        
    def get_parameter_vector(self):
        unitary_matrix=self.unitary_matrix
        n_row=unitary_matrix.shape[0]
        n_column=unitary_matrix.shape[1]
        parameter_vector=numpy.zeros(0)
        matrix_decomposition=numpy.zeros((0, n_row, n_row), dtype=complex)
        rotation_matrix=lambda theta:numpy.array([[numpy.sin(theta), numpy.cos(theta)],[-numpy.cos(theta),numpy.sin(theta)]])
        for column_index in range(n_column):
            column_vector=unitary_matrix[column_index:,column_index]
            column_angles=numpy.angle(column_vector)
            parameter_vector=numpy.append(parameter_vector, column_angles)
            D=numpy.ones(n_row, dtype=complex)            
            D[column_index:]=numpy.exp(-1.0j*column_angles)
            D=numpy.diag(D)
            unitary_matrix=(D).dot(unitary_matrix)
            D=(D).T.conj()
            matrix_decomposition=numpy.append(matrix_decomposition, D[numpy.newaxis], axis=0)
            for row_index in range(n_row-2,column_index-1,-1):
                a=numpy.real(unitary_matrix[row_index][column_index])
                b=numpy.real(unitary_matrix[row_index+1][column_index])
                theta=numpy.arctan2(a,b)
                parameter_vector=numpy.append(parameter_vector, [theta])
                rotation_mat=rotation_matrix(theta)
                given_matrix=numpy.eye(n_row)
                given_matrix[row_index:(row_index+2),row_index:(row_index+2)]=rotation_mat
                unitary_matrix=given_matrix.dot(unitary_matrix)
                given_matrix=given_matrix.T.conj()
                matrix_decomposition=numpy.append(matrix_decomposition, given_matrix[numpy.newaxis], axis=0)
        self.matrix_decomposition=matrix_decomposition
        self.parameter_vector=parameter_vector
        return parameter_vector

    def get_unitary_matrix(self):
        parameter_vector=self.parameter_vector
        n_row=self.matrix_shape[0]
        n_column=self.matrix_shape[1]
        matrix_decomposition=numpy.zeros((0, n_row, n_row), dtype=complex)
        rotation_matrix=lambda theta:numpy.array([[numpy.sin(theta), numpy.cos(theta)],[-numpy.cos(theta),numpy.sin(theta)]])
        vector_index=0
        for column_index in range(n_column):
            column_angles=parameter_vector[vector_index:vector_index+(n_row-column_index)]
            vector_index=vector_index+(n_row-column_index)
            D=numpy.ones(n_row, dtype=complex)
            D[column_index:]=numpy.exp(-1.0j*column_angles)
            D=numpy.diag(D).T.conj()
            matrix_decomposition=numpy.append(matrix_decomposition, D[numpy.newaxis], axis=0)
            for row_index in range(n_row-2,column_index-1,-1):
                theta=parameter_vector[vector_index]
                vector_index=vector_index+1
                rotation_mat=rotation_matrix(theta)
                given_matrix=numpy.eye(n_row)
                given_matrix[row_index:(row_index+2),row_index:(row_index+2)]=rotation_mat
                given_matrix=given_matrix.T.conj()
                matrix_decomposition=numpy.append(matrix_decomposition, given_matrix[numpy.newaxis], axis=0)
        self.matrix_decomposition=matrix_decomposition
        unitary_matrix=reduce(numpy.dot, matrix_decomposition)
        self.unitary_matrix=unitary_matrix
        return unitary_matrix


def generate_trained_data(directory_name):
    unitary_parametrizer=Unitary_Matrix_Parameterization()
    file_names=os.listdir(directory_name)
    trained_vectors=[]
    for file_name in file_names:
        relative_file_name=os.path.join(directory_name, file_name)
        data=numpy.load(relative_file_name)
        F_list=data[1]
        for F in F_list:
            delays, P=numpy.linalg.eig(F)
            unitary_parametrizer.set_matrix(P)
            P_param=unitary_parametrizer.get_parameter_vector()
            trained_vectors.append(P_param)
    trained_vectors=numpy.array(trained_vectors)
    return trained_vectors

class LBG_scalar_Quantizer():

    def __init__(self, code_books):
        self.code_books=code_books
    
    def get_quantized_vector(self, input_matrix):
        input_matrix_shape=input_matrix.shape[0]
        U=polar(input_matrix)[0]
        parameterizer=Unitary_Matrix_Parameterization()
        parameterizer.set_matrix(U)
        parameter_vector=parameterizer.get_parameter_vector()
        code_books=self.code_books
        quantized_vector=[]
        for i in range(code_books.shape[0]):
            codebook=code_books[i]
            diff_values=numpy.abs(codebook-parameter_vector[i])**2
            quantized_vector.append(codebook[numpy.argmin(diff_values)])

        quantized_vector=numpy.array(quantized_vector)
        parameterizer.set_parameter_vector(quantized_vector, input_matrix.shape)
        quantized_matrix=parameterizer.get_unitary_matrix()
        return quantized_matrix


class fiber_MIMO_frequency_response():

    def __init__(self, U_list):
        self.U_list=numpy.array(U_list)
        self.N_wavelengths=len(numpy.array(U_list))
        self.N_modes= (self.U_list).shape[1]

    def set_mode_multiplexer(self, source_array):
        N_wavelengths=self.N_wavelengths
        self.source_shapes=source_array
        self.source_tile=numpy.tile(source_array, (N_wavelengths,1,1))

    def set_mode_demultiplexer(self, detector_array):
        N_wavelengths=self.N_wavelengths
        self.detector_shapes=detector_array
        detector_array_H=detector_array.T.conj()
        self.detector_tile=numpy.tile(detector_array_H, (N_wavelengths,1,1))

    def get_MIMO_channel_response(self):
        U_list=self.U_list
        source_tile=self.source_tile
        detector_tile=self.detector_tile
        total_MIMO_channel=numpy.matmul(U_list, source_tile)
        total_MIMO_channel=numpy.matmul(detector_tile, total_MIMO_channel)
        return total_MIMO_channel
    

def find_quantized_PMs(PM, U_c, source_dict, detector_dict, dict_keys):
    
    for dict_key in dict_keys:
        file_name='./FMF_codebooks/'+dict_key+'.npy'
        codebooks=numpy.load(file_name)
        quantizer=LBG_scalar_Quantizer(codebooks)
        quantized_PM=quantizer.get_quantized_vector(PM)
        source_dict[dict_key]=quantized_PM
        quantized_op_PM=numpy.matmul(U_c, quantized_PM)
        quantized_op_PM=quantized_op_PM/numpy.linalg.norm(quantized_op_PM, axis=0)
        detector_dict[dict_key]= (numpy.linalg.inv(quantized_op_PM)).T.conj()
    return source_dict, detector_dict


kappa_value=1
bandwidth=12.00e9
directory_name='./FMF_frequency_data/kappa_'+str(kappa_value)+'/'+str(int(bandwidth*1.00e-9))
file_names=os.listdir(directory_name)
source_dict= {}
detector_dict={}
dict_keys=['kappa_'+str(kappa_value)+'_3bits', 'kappa_'+str(kappa_value)+'_4bits', 'GUE_4bits', 'GUE_3bits']
count=0
for file_name in file_names:
    print(file_name)
    count=count+1
    relative_file_name=os.path.join(directory_name, file_name)
    data=numpy.load(relative_file_name)
    U_c=data[0]
    F_c=data[1]
    U_list=numpy.array(numpy.array(data[2:]))
    N_frequency_samples=U_list.shape[0]
    PM_delay, PM=numpy.linalg.eig(F_c)
    output_PM=numpy.matmul(U_c, PM)
    source_dict, detector_dict=find_quantized_PMs(PM, U_c, source_dict, detector_dict, dict_keys)

    source_dict['ideal']=numpy.eye(U_list.shape[1])
    detector_dict['ideal']=(numpy.linalg.inv(U_c)).T.conj()
    source_dict['perfect']=PM
    detector_dict['perfect']=(numpy.linalg.inv(output_PM)).T.conj()

    for dict_key in source_dict:
        MIMO_response=fiber_MIMO_frequency_response(U_list)
        source_array=source_dict[dict_key]
        detector_array=detector_dict[dict_key]
        MIMO_response.set_mode_multiplexer(source_array)
        MIMO_response.set_mode_demultiplexer(detector_array)
        MIMO_channel=MIMO_response.get_MIMO_channel_response()
        numpy.save('./FMF_response_data/kappa_'+str(kappa_value)+'/'+str(int(bandwidth*1.00e-9))+'/'+dict_key+'_'+file_name.replace('.npy', ''), MIMO_channel)


    











