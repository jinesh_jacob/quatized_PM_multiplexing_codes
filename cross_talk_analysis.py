
# This code will plot the cross-talk in different type of frequency dependent MIMO channels 

import numpy
import matplotlib.pyplot as plt

bandwidth=12.00
start_frequency=-(bandwidth/2.0)
end_frequency=(bandwidth/2.0)
N_samples=1024
frequencies = numpy.linspace(start_frequency, end_frequency, num=N_samples, endpoint=False)
start_index=int(N_samples/2)-85
end_index=int(N_samples/2)+85

channel_index=0
result_dict={}
kappa_value=1
file_numbers= list(map(str, list(range(10))))
directory_name='./kappa_'+str(kappa_value)+'/'+str(int(bandwidth))+'/'
file_types=['GUE_3bits', 'GUE_4bits', 'ideal', 'kappa_'+str(kappa_value)+'_3bits', 'kappa_'+str(kappa_value)+'_4bits','perfect']
for file_type in file_types:
    channels=[]
    for file_number in file_numbers:
        file_name=directory_name+file_type+'_'+file_number+'.npy'
        channel=(numpy.load(file_name))
        channel=numpy.abs(channel)**2
        channels.append(channel)
    channels=numpy.array(channels)
    channel_avg=numpy.average(channels, axis=0)
    result_dict[file_type]=channel_avg

for file_type in file_types:
    channel=result_dict[file_type]
    direct_channel_arr=channel[:,channel_index, channel_index]
    cross_talk_indices=numpy.delete(numpy.arange(6, dtype=int), channel_index)
    cross_talk=[]
    for cross_talk_index in cross_talk_indices:
        cross_talk.append(channel[:,channel_index, cross_talk_index])
    cross_talk=numpy.array(cross_talk)
    cross_talk_total=numpy.sum(cross_talk,axis=0)
    signal_cross_talk=(cross_talk_total/direct_channel_arr)
    if file_type=='kappa_'+str(kappa_value)+'_3bits':
        plt.plot(frequencies[start_index:end_index], signal_cross_talk[start_index:end_index], 'rD-', label='3 bits/param. quantized PMs (Model)', marker='*', markevery=10, markeredgecolor='black',markeredgewidth=0.8)
    elif file_type=='kappa_'+str(kappa_value)+'_4bits':
         plt.plot(frequencies[start_index:end_index], signal_cross_talk[start_index:end_index], 'g^-',label='4 bits/param. quantized PMs (Model)', marker='o', markevery=10, markeredgecolor='black',markeredgewidth=0.8)
    elif file_type=='perfect':
        plt.plot(frequencies[start_index:end_index], signal_cross_talk[start_index:end_index], 'bo-', label='Unquantized PMs', marker='D', markevery=10, markeredgecolor='black',markeredgewidth=0.8)
    elif file_type=='ideal':
        plt.plot(frequencies[start_index:end_index], signal_cross_talk[start_index:end_index], 'kh-',label='Improved ideal modes', marker='^', markevery=10, markeredgecolor='black',markeredgewidth=0.8)
    elif file_type=='GUE_3bits':
        plt.plot(frequencies[start_index:end_index], signal_cross_talk[start_index:end_index], 'yx-', label='3 bits/param. quantized PMs (GUE)', marker='+', markevery=10, markeredgecolor='black',markeredgewidth=0.8)
    elif file_type=='GUE_4bits':
        plt.plot(frequencies[start_index:end_index], signal_cross_talk[start_index:end_index], 'cs-', label='4 bits/param. quantized PMs (GUE)', marker='H', markevery=10, markeredgecolor='black',markeredgewidth=0.8)
        
plt.gca().set_ylim([0.0, 0.00008])
plt.xlabel('Frequency in GHz')
plt.ylabel('Cross-talk channel response')
plt.legend(loc=9)
plt.savefig('kappa_'+str(kappa_value)+'_cross_talk_'+str(channel_index))
plt.close()

