import numpy
import pickle, os
import matplotlib.pyplot as plt

def get_coupling_values(mode_coupling_matrices, input_indices, output_indices):
    mode_coupling_matrices=numpy.array(mode_coupling_matrices)
    matrix_size=mode_coupling_matrices.shape[0]
    N_modes=mode_coupling_matrices.shape[1]
    input_matrix=numpy.zeros((N_modes,1))
    input_matrix[input_indices,:]=1.0
    input_matrix=input_matrix/numpy.linalg.norm(input_matrix)
    input_tile=numpy.tile(input_matrix, (matrix_size,1,1))
    output_matrices=numpy.matmul(mode_coupling_matrices, input_tile)
    output_matrices=output_matrices[:,output_indices,:]
    output_powers=numpy.linalg.norm(output_matrices, axis=1)
    output_powers=output_powers**2
    output_powers_dB=10*numpy.log10(output_powers)
    output_power_avg=numpy.average(output_powers_dB)
    return output_power_avg

input_indices=[1,2,4,5]
mode_coupling_matrices=[]
labels=['LP01', 'LP11']
colors=['rD-', 'g^-']
count=0
for output_index in [[0,3], [1,2,4,5]]:
    coupling_array=[]
    kappa_values=[1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 40.0, 60.0, 80.0, 100.0]
    for kappa_value in kappa_values:
        directory_name='./kappa_'+str(int(kappa_value))
        file_names=os.listdir(directory_name)
        coupling_matrices=numpy.zeros((0,6,6))
        for file_name in file_names:
            relative_file_name=directory_name+'/'+file_name
            data=numpy.load(relative_file_name)
            U_list=data[0]
            coupling_matrices=numpy.append(coupling_matrices, U_list, axis=0)
        coupling_value=get_coupling_values(coupling_matrices, input_indices, output_index)
        coupling_array.append(coupling_value)
    plt.plot(kappa_values, coupling_array, colors[count], label=labels[count],  markevery=2, markeredgecolor='black',markeredgewidth=0.8)
    count=count+1
        
plt.legend()
plt.title('Power launch in LP11')
plt.xlabel(r'$\sigma^2_{\kappa}$ (in $m^{-2}$)')
plt.ylabel('Mode coupling (in dB)')
plt.gca().set_ylim([-50.0,10.0])
plt.savefig('lp11')
plt.close()



