
# This code will calculate  the BER for different cross-talk channels
 
import numpy
from scipy import signal
from scipy.linalg import expm
import itertools
import pickle
import os, sys, fnmatch
import matplotlib.pyplot as plt
import timeit

#numpy.random.seed(20)

class Geodesic_Interpolator():

    def __init__(self, array_size=64, N_feedback_points=2, orientaion_flag=False, geodesic_indices=None):
        self.orientaion_flag=orientaion_flag
        self.array_size=array_size
        self.N_feedback_points=N_feedback_points
        self.set_Geodesic_points(geodesic_indices)

    def set_Geodesic_points(self, geodesic_indices=None):
        if geodesic_indices is None:
            N_feedback=self.N_feedback_points-2
            array_size=self.array_size
            start_index=0
            end_index=array_size-1
            n_unfilled_bins=end_index-start_index-1
            n_clusters=N_feedback+1
            min_n_bins_cluster= int((n_unfilled_bins+1)/n_clusters)
            n_cluster_more_elements=((n_unfilled_bins+1)%n_clusters)
            n_bins_cluster=[min_n_bins_cluster]*(n_clusters-n_cluster_more_elements)
            n_bins_cluster=numpy.append(n_bins_cluster, [min_n_bins_cluster+1]*n_cluster_more_elements)
            feedback_indices=numpy.array(start_index)
            feedback_indices= numpy.append(feedback_indices,start_index+numpy.cumsum(n_bins_cluster)[:n_clusters-1])
            feedback_indices=numpy.append(feedback_indices, end_index)
            self.geodesic_indices=feedback_indices.astype(int)
        else:
            self.geodesic_indices=geodesic_indices
            

    def apply_geodesic_interpolation(self, input_list):
        feedback_indices=self.geodesic_indices
        feedback_list=input_list
        feedback_indices_list=numpy.array(feedback_indices).tolist()
        V_interpolate_list=numpy.zeros((0, feedback_list[0].shape[0], feedback_list[0].shape[0]))
        for i in range(len(feedback_indices)-1):
            V_current=feedback_list[i]
            V_next=feedback_list[i+1]
            n_indices_to_be_filled=feedback_indices[i+1]-feedback_indices[i]-1
            if(i==len(feedback_indices)-2):
                V_interpolate=self.basic_Geodesic_interpolation(V_current, V_next, n_indices_to_be_filled, last_fill_flag=True)
            else:
                V_interpolate=self.basic_Geodesic_interpolation(V_current, V_next, n_indices_to_be_filled, last_fill_flag=False)
            V_interpolate_list = numpy.append(V_interpolate_list, V_interpolate, axis=0)
        return V_interpolate_list

    def basic_Geodesic_interpolation(self, V_current, V_next, n_indices_to_be_filled, last_fill_flag=False):
        orientaion_matrix=numpy.eye(V_current.shape[0])
        if(self.orientaion_flag):
            orientaion_matrix=self.find_orientation_matrix(V_current, V_next)
        M=numpy.matrix(numpy.linalg.inv(V_current))*numpy.matrix(V_next)*numpy.matrix(orientaion_matrix)
        sigma, A=numpy.linalg.eig(numpy.array(M))
        S_sigma=(numpy.log(sigma))
        S=numpy.matrix(A)*numpy.matrix(numpy.diag(S_sigma))*numpy.matrix(numpy.linalg.inv(A))
        t=numpy.linspace(0,1,num=n_indices_to_be_filled+1, endpoint=False)
        interpolation_fn= lambda  t:numpy.matrix(V_current)*numpy.matrix(expm(t*S))
        V_interpolate=list(map(interpolation_fn, t))
        if(last_fill_flag):
            V_interpolate.append(V_next)
        return numpy.array(V_interpolate)

    def find_orientation_matrix(self, V0, V1):
        A_current= numpy.diag(numpy.matmul(V1.T.conj(),V0))
        orientaion_matrix=numpy.diag((A_current/numpy.absolute(A_current)))
        return orientaion_matrix



class OOK_modulator():

    def __init__(self, sampling_period, sample_per_period, laser_power=-10.0):
        self.Ts=sampling_period
        self.sample_per_period=sample_per_period
        self.T= (self.sample_per_period)*(self.Ts)
        self.set_laser_power(laser_power)

    def set_laser_power(self, laser_power):
        self.laser_power=1.0e-3*(10**(laser_power/10.0))

    def set_number_channels(self, N_channels):
        self.N_channels=N_channels
        self.channel_power=self.laser_power/self.N_channels
        self.guass_amplitude=self.get_guassian_amplitude(self.T, self.channel_power)

    def get_guassian_amplitude(self, period, power):
        time_array, time_step=numpy.linspace(-period/2.0, period/2.0, num=128, retstep=True, endpoint=False)
        guass_sigma=period/6.0
        unit_gauss=numpy.exp(-0.5*(time_array/guass_sigma))
        number_symbols=10000
        rand_inf=numpy.random.randint(2, size=(number_symbols,1))
        moduluted_signal=(rand_inf*numpy.tile(unit_gauss, (number_symbols,1))).flatten()
        unit_power=(time_step*numpy.sum(moduluted_signal**2))/(number_symbols*period)
        amplitude=numpy.sqrt(power/unit_power)
        return amplitude

    def set_information_sequence(self, inf_arr):
        self.inf_arr=inf_arr
        self.N_symbols=inf_arr.shape[1]
        self.set_number_channels(inf_arr.shape[0])

    def get_guassian_pulse(self):
        period=self.T
        N_points=self.sample_per_period
        amplitude=self.guass_amplitude
        time_array=numpy.linspace((-1.0)*period/2.0, period/2.0, num=N_points, endpoint=False)
        guass_sigma=period/6.0
        guass_pulse=amplitude*numpy.exp(-0.5*(time_array/guass_sigma)**2)
        return guass_pulse

    def get_OOK_modulated_output(self):
        N_symbols=self.N_symbols
        guass_pulse=self.get_guassian_pulse()
        guass_tile=numpy.tile(guass_pulse, (N_symbols,1))
        inf_arr=self.inf_arr
        modulated_output=[((inf_arr[[i],:].T)*guass_tile).flatten() for i in range(inf_arr.shape[0])]
        return numpy.array(modulated_output)


class optical_MIMO_channel():

    def __init__(self, U_list):
        self.U_list=U_list
        frequency_responses=numpy.array([U_list[:,i,j] for i,j in itertools.product( range(U_list.shape[1]), range(U_list.shape[2]))])
        self.frequency_responses=frequency_responses
        self.impulse_size=U_list.shape[0]
        
    def get_impulse_response(self, i,j):        
        U_list=self.U_list
        frequency_responses=self.frequency_responses
        frequency_response=frequency_responses[i*(U_list.shape[1])+j]
        frequency_response=(numpy.hanning(frequency_response.shape[0]))*frequency_response
        frequency_response=numpy.fft.fftshift(frequency_response)
        impulse_response=numpy.fft.ifft(frequency_response)
        impulse_response=numpy.fft.fftshift(impulse_response)
        return impulse_response

    def set_input_signal(self, input_signal):
        self.input_signal=input_signal

    def calculate_significant_cross_talk_channels(self, N_channels, threshold=0.1):
        U_list=self.U_list
        channels_indices=numpy.arange(N_channels)
        cross_talk_indices_total=[]
        for channel_index in channels_indices:
            cross_talk_indices=numpy.delete(channels_indices, channel_index)
            cross_talk_energy=[]
            for cross_talk_index in cross_talk_indices:
                signal_freq=U_list[:,channel_index,cross_talk_index]
                cross_talk_energy.append([cross_talk_index, numpy.sum(numpy.abs(signal_freq)**2)])
            cross_talk_energy=numpy.array(cross_talk_energy)
            highest_cross_talk=numpy.amax(cross_talk_energy[:,1])
            indices=cross_talk_energy[:,0][cross_talk_energy[:,1]>highest_cross_talk*threshold]
            cross_talk_indices_total.append(indices.astype(int))
        return cross_talk_indices_total
            
    def get_output_signal(self):
        input_signal=self.input_signal
        impulse_size=self.impulse_size
        output_signals=[]
        # for i in range(input_signal.shape[0]):
        #     output_signal=[]
        #     for j in range(input_signal.shape[0]):                
        #         output_signal.append(signal.fftconvolve(input_signal[j], self.get_impulse_response(i,j), mode='full'))
        #     output_signal=numpy.sum(output_signal, axis=0)
        #     output_signals.append(output_signal)
        
        N_channels=input_signal.shape[0]
        significant_cross_talk_indices=self.calculate_significant_cross_talk_channels(N_channels)
        for channel_index in range(N_channels):
            output_signal=[]
            direct_signal=signal.fftconvolve(input_signal[channel_index], self.get_impulse_response(channel_index,channel_index), mode='full')
            output_signal.append(direct_signal)
            for cross_talk_index in significant_cross_talk_indices[channel_index]:
                cross_talk_signal=signal.fftconvolve(input_signal[cross_talk_index], self.get_impulse_response(channel_index, cross_talk_index), mode='full')
                output_signal.append(cross_talk_signal)
            output_signal=numpy.sum(output_signal, axis=0)
            output_signals.append(output_signal)    
        output_signals=numpy.array(output_signals)
        return output_signals

class OOK_demodulator():

    def __init__(self, sample_per_symbol, N_symbols):
        self.sample_per_symbol=sample_per_symbol
        self.N_symbols=N_symbols

    def set_channel_out(self, channel_out):
        self.channel_out=channel_out
        self.N_channels=channel_out.shape[0]

    def set_pilot_signals(self, pilot_signals):
        N_channels=self.N_channels
        self.pilot_signals=pilot_signals
        self.pilot_signal_size=N_channels*(pilot_signals.shape[1])
        self.channel_delays=self.get_channel_delays()
        self.pilot_flag=True

    def set_pilot_flag(self, pilot_flag=False):
         self.pilot_flag=pilot_flag

    def get_delay_index(self, matched_out, channel_out_size):
        N_symbols=self.N_symbols
        sample_per_symbol=self.sample_per_symbol
        total_arr_size=N_symbols*sample_per_symbol
        high_match_indices=numpy.argsort(numpy.abs(matched_out))[::-1]
        check_indices=(channel_out_size-high_match_indices)-total_arr_size
        delay_index=numpy.nonzero(check_indices>0)[0][0]
        return high_match_indices[delay_index]
         
    def get_channel_delays(self):
        channel_out=self.channel_out
        N_channels=self.N_channels
        pilot_signals=self.pilot_signals
        N_symbols=self.N_symbols
        sample_per_symbol=self.sample_per_symbol
        delay_indices=[]
        for i in range(N_channels):
            pilot_signal=numpy.abs(pilot_signals[i])
            channel_arr=numpy.abs(channel_out[i])
            matched_out=signal.fftconvolve(channel_arr, pilot_signal[::-1], mode='valid')
            # delay_index=numpy.argmax(numpy.abs(matched_out))
            delay_index=self.get_delay_index(matched_out, len(channel_arr))
            delay_index=delay_index-(i*pilot_signal.shape[0])
            delay_indices.append(delay_index)
        delay_indices=numpy.array(delay_indices)
        return delay_indices

    def get_demodulated_signal_indices(self):
        pilot_signal_size=self.pilot_signal_size
        N_symbols=self.N_symbols
        sample_per_symbol=self.sample_per_symbol
        N_channels=self.N_channels
        pilot_flag=self.pilot_flag
        channel_delays=self.channel_delays
        indices=[]
        for i in range(N_channels):
            start_index=channel_delays[i]
            end_index=start_index+ (N_symbols*sample_per_symbol)
            indices.append([start_index, end_index])
        indices=numpy.array(indices)
        if pilot_flag:
            indices=indices+pilot_signal_size
        return indices

    def get_demodulated_signal(self):
        sample_per_symbol=self.sample_per_symbol
        N_symbols=self.N_symbols
        N_channels=self.N_channels
        channel_out=self.channel_out
        relevant_indices=self.get_demodulated_signal_indices()
        signal_arr=[]
        for i in range(N_channels):
            index=relevant_indices[i]
            start_index=index[0]
            end_index=index[1]
            signal_arr.append(channel_out[i][start_index:end_index])
        signal_arr=numpy.array(signal_arr)
        signal_arr= numpy.abs(signal_arr)**2
        demodulated_signal=numpy.sum(signal_arr.reshape(N_channels, N_symbols, sample_per_symbol), axis=2)
        return demodulated_signal

class OOK_detection():

    def __init__(self, inf_arr, demodulated_arr):
        self.inf_arr=inf_arr
        self.demodulated_arr=demodulated_arr

    def get_threshold_value(self):
        inf_arr=self.inf_arr
        demodulated_arr=self.demodulated_arr
        one_threshold=numpy.average(inf_arr*demodulated_arr)
        zero_threshold=numpy.average(numpy.abs(1.0-inf_arr)*demodulated_arr)
        threshold_value=0.5*(one_threshold + zero_threshold)
        return threshold_value

    def get_output_sequence(self):
        threshold_value=self.get_threshold_value()
        demodulated_signal=numpy.copy(self.demodulated_arr)
        demodulated_signal[demodulated_signal>threshold_value]=1.0
        demodulated_signal[demodulated_signal<=threshold_value]=0.0
        return demodulated_signal

    def get_number_errors(self):
        input_sequence=self.inf_arr
        output_sequence=self.get_output_sequence()
        output_sequence=output_sequence.astype( int)
        N_errors=numpy.sum(numpy.abs(output_sequence-input_sequence))
        return N_errors

class OOK_BER_calculator():

    def __init__(self, channel_frequency_list, bandwidth_measured):
        channel_frequency_list=numpy.array(channel_frequency_list)
        self.channel_list=channel_frequency_list
        self.bandwidth_measured_oneside=bandwidth_measured/2.0
        self.N_sample_points= channel_frequency_list.shape[0]
        freq_arr, freq_step=numpy.linspace((-1.0)*(bandwidth_measured/2.0), (bandwidth_measured/2.0), num=self.N_sample_points, endpoint=False, retstep=True)
        self.freq_step_measured=freq_step
        self.freq_arr_measured=freq_arr

    def set_bit_rate(self, bit_rate, N_sample_per_symbol=20, impulse_size=128):
        self.bit_rate=bit_rate
        self.N_sample_per_symbol=N_sample_per_symbol
        self.impulse_size=impulse_size
        self.symbol_duration=(1.0/bit_rate)
        self.sampling_time=(self.symbol_duration)/(self.N_sample_per_symbol)

    def set_pilot_size(self, pilot_size):
        self.pilot_size=pilot_size
        
    def set_laser_power(self, laser_power):
        self.laser_power=laser_power

    def get_measured_indices(self, freq_arr_measured, freq_arr_req):
        indices_returned=None
        if(len(freq_arr_measured)<len(freq_arr_req)):
            indices=[]
            for index in range(freq_arr_measured.shape[0]):
                frequency_mesd=freq_arr_measured[index]
                indices.append(numpy.argmin(numpy.abs(freq_arr_req-frequency_mesd)))
            indices_returned=numpy.sort(numpy.array(indices))
        else:
            indices=[]
            for index in range(freq_arr_req.shape[0]):
                frequency_reqd=freq_arr_req[index]
                indices.append(numpy.argmin(numpy.abs(freq_arr_measured-frequency_reqd)))
            indices_returned=numpy.sort(numpy.array(indices))
        return indices_returned
    
    def get_adjust_channel_list(self):
        channel_frequency_list=self.channel_list
        N_modes=channel_frequency_list.shape[1]
        impulse_size=self.impulse_size
        bandwidth_measured_oneside=self.bandwidth_measured_oneside
        sampling_time=self.sampling_time
        bandwidth_required=(1.0/(2.0*sampling_time))
        freq_arr_req=numpy.linspace(-(1.0)*bandwidth_required, bandwidth_required, num=impulse_size, endpoint=False)
        freq_arr=self.freq_arr_measured
        condition_arr=numpy.logical_and(freq_arr>=-bandwidth_required, freq_arr<=bandwidth_required)
        freq_arr_measured=freq_arr[condition_arr]
        measrd_indices=self.get_measured_indices(freq_arr_measured, freq_arr_req)
        adjusted_list=numpy.zeros((impulse_size, N_modes, N_modes), dtype=complex)
        channel_list_slice=channel_frequency_list[condition_arr]
        if(len(freq_arr_measured)<len(freq_arr_req)):
            measrd_indices_inter=measrd_indices-measrd_indices[0]
            interpolator=Geodesic_Interpolator(measrd_indices_inter[-1]+1, len(measrd_indices_inter), False, measrd_indices_inter)
            inter_list=interpolator.apply_geodesic_interpolation(channel_list_slice)
            adjusted_list[measrd_indices[0]:measrd_indices[-1]+1]=inter_list
        else:
            adjusted_list=channel_list_slice[measrd_indices]
        return adjusted_list
    
    def generate_pilot_symbols(self, N_channles, pilot_size):
        pilot_symbols=numpy.zeros((N_channles, 0), dtype=int)
        for i in range(N_channles):
            rand_symbols=numpy.random.randint(2, size=pilot_size)
            pilot_arr=numpy.zeros((N_channles, pilot_size), dtype=int)
            pilot_arr[i]=rand_symbols
            pilot_symbols=numpy.append(pilot_symbols, pilot_arr, axis=1)
        return pilot_symbols

    def get_pilot_signals(self, modulated_output, pilot_size, sample_per_symbol):
        N_channels=modulated_output.shape[0]
        pilot_signals=[]
        for i in range(N_channels):
            start_index=i*pilot_size*sample_per_symbol
            end_index=(i+1)*pilot_size*sample_per_symbol
            pilot_signal=modulated_output[i][start_index:end_index]
            pilot_signals.append(pilot_signal)
        return numpy.array(pilot_signals)

    def get_BER(self):
        
        adjusted_list=self.get_adjust_channel_list()
        pilot_size=self.pilot_size
        N_channles=adjusted_list.shape[1]
        sampling_time=self.sampling_time
        N_sample_per_symbol=self.N_sample_per_symbol
        laser_power=self.laser_power
        N_bits_iteration=1000000
        start_time=timeit.default_timer()
        
        ###### Creating modulator ############
        moduluter=OOK_modulator(sampling_time, N_sample_per_symbol, laser_power=laser_power)
            
        ######### pilot adding ############
        info_arr=numpy.random.randint(2, size=(N_channles, N_bits_iteration))
        pilot_symbols=self.generate_pilot_symbols(N_channles, pilot_size)
        total_arr=numpy.append(pilot_symbols, info_arr, axis=1)
        moduluter.set_information_sequence(total_arr)
        modulated_output=moduluter.get_OOK_modulated_output()
        pilot_signals=self.get_pilot_signals(modulated_output, pilot_size, N_sample_per_symbol)

        ###### Signal passing through channel##########
        fiber_channel=optical_MIMO_channel(adjusted_list)
        fiber_channel.set_input_signal(modulated_output)
        channel_out=fiber_channel.get_output_signal()
        
        ##### Demodulator part ##############
        demodulator= OOK_demodulator(N_sample_per_symbol, N_bits_iteration)
        demodulator.set_channel_out(channel_out)
        demodulator.set_pilot_signals(pilot_signals)
        demodulated_signal=demodulator.get_demodulated_signal()
        #### Detection ##############
        error_detector= OOK_detection(info_arr, demodulated_signal)
        N_errors=error_detector.get_number_errors()
        end_time=timeit.default_timer()
        print(end_time-start_time)
        BER= (N_errors)/(N_bits_iteration*N_channles)
        return BER
        

def calculate_BER_data_rate(channel, bandwidth_measured, data_rate, sample_size):
    BER_list=[]
    data_rates= (bandwidth_measured*0.001)*numpy.random.randn(sample_size)+data_rate
    for data_rate in data_rates:
        calculator=OOK_BER_calculator(channel, bandwidth_measured)
        calculator.set_bit_rate(bit_rate=data_rate, N_sample_per_symbol=4, impulse_size=1024)
        calculator.set_laser_power(-20.0)
        calculator.set_pilot_size(500)
        BER=calculator.get_BER()
        BER_list.append(BER)
    BER=numpy.median(BER_list)
    return BER


def validate_the_current_loop(file_number, file_type, kappa_value,directory_name):
    validate_flag=False
    try:
        file_name=directory_name+'/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy'
        result_arr=numpy.load(file_name)
    except IOError:
        validate_flag=True
    return validate_flag
    
bandwidth=12.00e9
data_rate=10.0e9
for file_number in ['8', '9', '10', '11', '12']:
    for file_type in ['perfect', 'ideal', 'trained_4', 'trained_3', 'GUE_4']:
        kappa_vals=[1, 6, 10, 14, 20]
        for kappa_value in kappa_vals:
            validate_flag=validate_the_current_loop(file_number, file_type, kappa_value,'./FMF_result_coupling')
            print(validate_flag, file_number, file_type, kappa_value)
            if validate_flag:                
                full_file_name=None
                if file_type=='perfect':
                    full_file_name='./FMF_response_data/kappa_'+str(kappa_value)+'/12/perfect_'+file_number+'.npy'
                elif file_type=='ideal':
                    full_file_name='./FMF_response_data/kappa_'+str(kappa_value)+'/12/ideal_'+file_number+'.npy'
                elif file_type=='trained_4':
                    full_file_name='./FMF_response_data/kappa_'+str(kappa_value)+'/12/kappa_'+str(kappa_value)+'_4bits_'+file_number+'.npy'
                elif file_type=='trained_3':
                    full_file_name='./FMF_response_data/kappa_'+str(kappa_value)+'/12/kappa_'+str(kappa_value)+'_3bits_'+file_number+'.npy'
                elif file_type=='GUE_3':
                    full_file_name='./FMF_response_data/kappa_'+str(kappa_value)+'/12/GUE_3bits_'+file_number+'.npy'
                elif file_type=='GUE_4':
                    full_file_name='./FMF_response_data/kappa_'+str(kappa_value)+'/12/GUE_4bits_'+file_number+'.npy'

                channel=numpy.load(full_file_name)
                BER=calculate_BER_data_rate(channel, bandwidth_measured=bandwidth, data_rate=data_rate, sample_size=29)
                numpy.save('./FMF_result_coupling/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy', numpy.array([BER]))

                
