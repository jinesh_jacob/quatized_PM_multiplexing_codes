import fiber
import numpy
import timeit
import scipy.constants as const
import pickle, os

#numpy.random.seed(10)

NA = 0.14
a = 6.5e-06;
alpha=2.00
length=1000.0
n_sections=5000
step_length=(length/float(n_sections))
sigma_kappa= 20.0
sigma_theta=0.36
number_simulations=500
start_file_number=0

for simulation_index in range(start_file_number, start_file_number+number_simulations):
    print(simulation_index)
    start=timeit.default_timer()
    U_list=[]
    F_list=[]
    for list_index in range(20):
        fiber_obj=fiber.LargeCoreMMF(length=length, step_length=step_length, NA=NA, a=a, alpha=alpha)
        kappa_vals=numpy.abs(sigma_kappa*numpy.random.randn(n_sections))
        theta_vals=sigma_theta*numpy.random.randn(n_sections)
        wavelength=1.55e-6
        fiber_obj.set_wavelength(wavelength)
        fiber_obj.set_kappa_vals_theta_vals(kappa_vals=kappa_vals, theta_vals=theta_vals)
        U=fiber_obj.calculate_matrix()
        f_step=1.089e+8  #1.095e+6
        omega_step=2*numpy.pi*f_step
        omega=2*numpy.pi*(const.c/wavelength)
        wavelength_step=(2*numpy.pi*const.c)/(omega+omega_step)
        fiber_obj.set_wavelength(wavelength_step)
        U_step=fiber_obj.calculate_matrix()
        U_diff=(U_step-U)/omega_step
        F= (1.0j)*numpy.dot(U.T.conj(), U_diff)
        PM_delay, PM=numpy.linalg.eig(F)
        U_list.append(U)
        F_list.append(F)
    stop=timeit.default_timer()
    print (stop-start)
    output_list=numpy.array([U_list, F_list])
    file_name='./kappa_'+str(int(sigma_kappa))+'/'+str(simulation_index)
    numpy.save(file_name, output_list)
