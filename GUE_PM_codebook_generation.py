import numpy
import pickle

class scalar_LBG_algorithm():

    def __init__(self, data, n_bits):
        self.data_vectors=data
        self.bits=n_bits

    def get_LBG_codebook(self):
        data_vectors=self.data_vectors
        bits=self.bits
        N_data_vectors= len(data_vectors)
        code_vectors_bin={}
        bins={}
        step=0.0001
        code_vectors=numpy.zeros(0)
        code_vectors=numpy.append(code_vectors, self.find_centroid(data_vectors))
        code_vectors_bin[0]=code_vectors
        bins[0]=data_vectors
        D_avg= self.find_distortion(data_vectors,code_vectors[0])
        N_code_vectors=len(code_vectors)
        for itr in range(bits):
            for code_index in range(N_code_vectors):
                splitted_code_vectors=self.split_code_word(code_vectors[code_index],step)
                code_vectors[code_index]=splitted_code_vectors[0]
                code_vectors=numpy.append(code_vectors, splitted_code_vectors[1])
                bins[N_code_vectors+code_index]=numpy.zeros(0)
            N_code_vectors=len(code_vectors)
            while True:
                for bin_index in range(N_code_vectors):
                    N_bin_vectors= len(bins[bin_index])
                    removable_indices=[]
                    for bin_vector_index in range(N_bin_vectors):
                        current_data_vector=(bins[bin_index])[bin_vector_index]
                        min_code_vector_index=self.find_shortest_distance_code_word(current_data_vector, code_vectors)
                        if min_code_vector_index != bin_index:
                             bins[min_code_vector_index]=numpy.append(bins[min_code_vector_index],current_data_vector)
                             removable_indices.append(bin_vector_index)
                    bins[bin_index]=numpy.delete(bins[bin_index],removable_indices)
                D_avg_temp=0.0
                for bin_index in range(N_code_vectors):
                    if len(bins[bin_index]):
                        code_vectors[bin_index]= self.find_centroid(bins[bin_index])
                        D_avg_temp=D_avg_temp + (len(bins[bin_index]))*self.find_distortion(bins[bin_index],code_vectors[bin_index])
                D_avg_temp=(D_avg_temp/N_data_vectors)
                K=(D_avg-D_avg_temp)/(D_avg)
                if K < step or numpy.isnan(K):
                    break
                D_avg=D_avg_temp
            code_vectors_bin[itr+1]=code_vectors    
        return code_vectors_bin


    def find_centroid(self, data_vectors):
        geometric_mean= numpy.mean(data_vectors)
        return geometric_mean


    def find_distortion(self, data_vectors, code_vector):
        difference_vectors=data_vectors-code_vector
        frob_norm_vals=numpy.abs(difference_vectors)**2
        error_mean=numpy.mean(frob_norm_vals)
        return error_mean


    def find_shortest_distance_code_word(self, data_vector,code_vectors):
        difference_vectors=code_vectors-data_vector
        frob_norm_vals=numpy.abs(difference_vectors)**2
        return numpy.argmin(frob_norm_vals)

    def split_code_word(self, code_vector, step):
        split_1= (1+step)*code_vector
        split_2= (1-step)*code_vector
        return split_1, split_2



class Unitary_Matrix_Parameterization():

    def __init__(self, unitary_matrix=numpy.zeros((0,0)), parameter_vector=numpy.zeros(0), matrix_shape=(0,0)):
        self.matrix_decomposition=None
        self.matrix_shape=matrix_shape
        if(numpy.all(unitary_matrix.shape!=(0,0))):
            self.set_matrix(unitary_matrix)
        if(numpy.all(parameter_vector.size!=0)):
            self.set_parameter_vector(parameter_vector, matrix_shape)

    def set_matrix(self, input_matrix):
        self.unitary_matrix=input_matrix
        self.parameter_vector=numpy.zeros(0)
        self.matrix_shape=input_matrix.shape

    def set_parameter_vector(self, parameter_vector, matrix_shape):
        self.matrix_shape=matrix_shape
        self.parameter_vector=parameter_vector
        self.unitary_matrix=numpy.zeros((0,0))
        
        
    def get_parameter_vector(self):
        unitary_matrix=self.unitary_matrix
        n_row=unitary_matrix.shape[0]
        n_column=unitary_matrix.shape[1]
        parameter_vector=numpy.zeros(0)
        matrix_decomposition=numpy.zeros((0, n_row, n_row), dtype=complex)
        rotation_matrix=lambda theta:numpy.array([[numpy.sin(theta), numpy.cos(theta)],[-numpy.cos(theta),numpy.sin(theta)]])
        for column_index in range(n_column):
            column_vector=unitary_matrix[column_index:,column_index]
            column_angles=numpy.angle(column_vector)
            parameter_vector=numpy.append(parameter_vector, column_angles)
            D=numpy.ones(n_row, dtype=complex)            
            D[column_index:]=numpy.exp(-1.0j*column_angles)
            D=numpy.diag(D)
            unitary_matrix=(D).dot(unitary_matrix)
            D=(D).T.conj()
            matrix_decomposition=numpy.append(matrix_decomposition, D[numpy.newaxis], axis=0)
            for row_index in range(n_row-2,column_index-1,-1):
                a=numpy.real(unitary_matrix[row_index][column_index])
                b=numpy.real(unitary_matrix[row_index+1][column_index])
                theta=numpy.arctan2(a,b)
                parameter_vector=numpy.append(parameter_vector, [theta])
                rotation_mat=rotation_matrix(theta)
                given_matrix=numpy.eye(n_row)
                given_matrix[row_index:(row_index+2),row_index:(row_index+2)]=rotation_mat
                unitary_matrix=given_matrix.dot(unitary_matrix)
                given_matrix=given_matrix.T.conj()
                matrix_decomposition=numpy.append(matrix_decomposition, given_matrix[numpy.newaxis], axis=0)
        self.matrix_decomposition=matrix_decomposition
        self.parameter_vector=parameter_vector
        return parameter_vector

    def get_unitary_matrix(self):
        parameter_vector=self.parameter_vector
        n_row=self.matrix_shape[0]
        n_column=self.matrix_shape[1]
        matrix_decomposition=numpy.zeros((0, n_row, n_row), dtype=complex)
        rotation_matrix=lambda theta:numpy.array([[numpy.sin(theta), numpy.cos(theta)],[-numpy.cos(theta),numpy.sin(theta)]])
        vector_index=0
        for column_index in range(n_column):
            column_angles=parameter_vector[vector_index:vector_index+(n_row-column_index)]
            vector_index=vector_index+(n_row-column_index)
            D=numpy.ones(n_row, dtype=complex)
            D[column_index:]=numpy.exp(-1.0j*column_angles)
            D=numpy.diag(D).T.conj()
            matrix_decomposition=numpy.append(matrix_decomposition, D[numpy.newaxis], axis=0)
            for row_index in range(n_row-2,column_index-1,-1):
                theta=parameter_vector[vector_index]
                vector_index=vector_index+1
                rotation_mat=rotation_matrix(theta)
                given_matrix=numpy.eye(n_row)
                given_matrix[row_index:(row_index+2),row_index:(row_index+2)]=rotation_mat
                given_matrix=given_matrix.T.conj()
                matrix_decomposition=numpy.append(matrix_decomposition, given_matrix[numpy.newaxis], axis=0)
        self.matrix_decomposition=matrix_decomposition
        unitary_matrix=reduce(numpy.dot, matrix_decomposition)
        self.unitary_matrix=unitary_matrix
        return unitary_matrix


def genearate_GUE_PM_data(sample_size, N_modes, variance):
    arr_diag= lambda x:numpy.array(list(map(numpy.diag, x)))
    A=numpy.sqrt(variance)*numpy.random.randn(sample_size, N_modes,N_modes) + (1.0j)*(numpy.sqrt(variance))**numpy.random.randn(sample_size, N_modes,N_modes)
    F=0.5*( A + numpy.transpose(A, (0,2,1)).conj())
    F_diag=arr_diag(F)
    F=F-arr_diag(F_diag)
    F_diag_mean=numpy.mean(F_diag, axis=1)
    F_diag=F_diag-F_diag_mean[:,numpy.newaxis]
    F=F+arr_diag(F_diag)
    PM_delay, PM=numpy.linalg.eig(F)
    return PM

def generate_trained_data(PM_list):
    trained_vectors=[]
    for P in PM_list:
        unitary_parametrizer=Unitary_Matrix_Parameterization()
        unitary_parametrizer.set_matrix(P)
        P_param=unitary_parametrizer.get_parameter_vector()
        trained_vectors.append(P_param)
    return numpy.array(trained_vectors)


def get_LBG_scalar_codebook(trained_vectors, n_bits):
    codebooks=numpy.zeros((0, 2**n_bits))
    for i in range(trained_vectors.shape[1]):
        scalar_quantizer=scalar_LBG_algorithm(data=trained_vectors[:,i], n_bits=n_bits)
        codebook=(scalar_quantizer.get_LBG_codebook())[n_bits]
        codebooks=numpy.append(codebooks, codebook[numpy.newaxis], axis=0)
    return codebooks

n_bits=3
PM_list=genearate_GUE_PM_data(20000, 6, 0.5)
trained_data=generate_trained_data(PM_list)
codebooks=numpy.zeros((0, 2**n_bits))
for i in range(trained_data.shape[1]):
    print('codebook_number ', i)
    scalar_quantizer=scalar_LBG_algorithm(data=trained_data[:,i], n_bits=n_bits)
    codebook=(scalar_quantizer.get_LBG_codebook())[n_bits]
    codebooks=numpy.append(codebooks, codebook[numpy.newaxis], axis=0)

file_name='GUE_'+str(n_bits)+'bits'
numpy.save(file_name, codebooks)

