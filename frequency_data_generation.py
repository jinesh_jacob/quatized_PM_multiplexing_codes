import fiber
import numpy
import timeit
import scipy.constants as const
import os



NA = 0.14
a = 6.5e-06;
alpha=2.00
length=1000.0
n_sections=5000
step_length=(length/float(n_sections))
sigma_kappa= 100.0
sigma_theta=0.36
number_simulations=10
start_file_number=200
bandwidth=2.00e+9
N_samples=1024
for simulation_index in range(start_file_number, start_file_number+number_simulations):
    print(simulation_index)
    numpy.random.seed(simulation_index)
    start=timeit.default_timer()
    kappa_vals=numpy.abs(sigma_kappa*numpy.random.randn(n_sections))
    theta_vals=sigma_theta*numpy.random.randn(n_sections)
    wavelength_c=1.55e-6
    start_frequency=(const.c/wavelength_c)-(bandwidth/2.0)
    end_frequency=(const.c/wavelength_c)+(bandwidth/2.0)
    frequencies = numpy.linspace(start_frequency, end_frequency, num=N_samples, endpoint=False)
    wavelengths=(const.c/frequencies)
    U_list=[]
    F_list=[]
    for wavelength in wavelengths:
        fiber_obj=fiber.LargeCoreMMF(length=length, step_length=step_length, NA=NA, a=a, alpha=alpha)
        fiber_obj.set_kappa_vals_theta_vals(kappa_vals=kappa_vals, theta_vals=theta_vals)
        fiber_obj.set_wavelength(wavelength)
        U=fiber_obj.calculate_matrix()
        U_list.append(U)
    fiber_obj=fiber.LargeCoreMMF(length=length, step_length=step_length, NA=NA, a=a, alpha=alpha)
    fiber_obj.set_kappa_vals_theta_vals(kappa_vals=kappa_vals, theta_vals=theta_vals)
    fiber_obj.set_wavelength(wavelength_c)
    U_c=fiber_obj.calculate_matrix()
    f_step=1.0+10
    omega_step=2*numpy.pi*f_step
    omega=2*numpy.pi*(const.c/wavelength_c)
    wavelength_step=2*numpy.pi*(const.c/(omega+omega_step))
    fiber_obj=fiber.LargeCoreMMF(length=length, step_length=step_length, NA=NA, a=a, alpha=alpha)
    fiber_obj.set_kappa_vals_theta_vals(kappa_vals=kappa_vals, theta_vals=theta_vals)
    fiber_obj.set_wavelength(wavelength_step)
    U_c_step=fiber_obj.calculate_matrix()
    U_diff=(U_c_step-U_c)/omega_step
    F_c= (1.0j)*numpy.dot(U_c.T.conj(), U_diff)
    PM_delay, PM=numpy.linalg.eig(F_c)
    stop=timeit.default_timer()
    print (stop-start)
    saved_data=numpy.append(U_c[numpy.newaxis], F_c[numpy.newaxis], axis=0)
    saved_data=numpy.append(saved_data, U_list, axis=0)
    file_name='./kappa_'+str(int(sigma_kappa))+'/'+str(int(bandwidth*1.0e-9))+'/'+str(simulation_index)
    numpy.save(file_name, saved_data)


