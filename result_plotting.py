import numpy
from scipy import signal
from scipy.linalg import expm
import itertools
import pickle
import os, sys, fnmatch
import matplotlib.pyplot as plt
import timeit

kappa_vals=[1, 6, 10, 14, 20]
ideal_BER_total=[]
perfect_BER_total=[]
trained_3_BER_total=[]
trained_4_BER_total=[]
GUE_4_BER_total=[]

for file_number in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16']:
    ideal_BER=[]
    perfect_BER=[]
    trained_3_BER=[]
    trained_4_BER=[]
    GUE_4_BER=[]
    for file_type in ['ideal', 'trained_3', 'trained_4', 'perfect' , 'GUE_4']:
        for kappa_value in kappa_vals:
            if file_type=='perfect':
                full_file_name='./FMF_result_coupling/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy'
                BER=numpy.load(full_file_name)
                perfect_BER.append(BER[0])
            elif file_type=='ideal':
                full_file_name='./FMF_result_coupling/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy'
                BER=numpy.load(full_file_name)
                ideal_BER.append(BER[0])
            elif file_type=='trained_4':
                full_file_name='./FMF_result_coupling/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy'
                BER=numpy.load(full_file_name)
                trained_4_BER.append(BER[0])
            elif file_type=='trained_3':
                full_file_name='./FMF_result_coupling/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy'
                BER=numpy.load(full_file_name)
                trained_3_BER.append(BER[0])
            elif file_type=='GUE_4':
                full_file_name='./FMF_result_coupling/'+file_number+'_'+file_type+'_'+str(kappa_value)+'.npy'
                BER=numpy.load(full_file_name)
                GUE_4_BER.append(BER[0])
        
                ideal_BER_total.append(ideal_BER)
    perfect_BER_total.append(perfect_BER)
    trained_3_BER_total.append(trained_3_BER)
    trained_4_BER_total.append(trained_4_BER)
    GUE_4_BER_total.append(GUE_4_BER)
plt.semilogy(kappa_vals, numpy.average(GUE_4_BER_total, axis=0), 'cs-', label='4 bits/param. quantized PMs (GUE)', markeredgecolor='black',markeredgewidth=0.8)
plt.semilogy(kappa_vals, numpy.average(ideal_BER_total, axis=0), 'kh-', label='Improved ideal modes', markeredgecolor='black',markeredgewidth=0.8)
plt.semilogy(kappa_vals, numpy.average(trained_3_BER_total, axis=0), 'rD-', label='3 bits/param. quantized PMs (Model)', markeredgecolor='black',markeredgewidth=0.8)
plt.semilogy(kappa_vals, numpy.average(trained_4_BER_total, axis=0), 'g^-', label='4 bits/param. quantized PMs (Model)', markeredgecolor='black',markeredgewidth=0.8)
plt.semilogy(kappa_vals, numpy.average(perfect_BER_total, axis=0), 'bo-', label='Unquantized PMs', markeredgecolor='black',markeredgewidth=0.8)


plt.legend()
plt.xlabel(r'$\sigma_{\kappa}^2$ (in \rpsquare\meter)')
plt.ylabel('BER')
plt.gca().set_ylim([1.0e-4,1.0e-0])
plt.savefig('BER_vs_kappa')
plt.close()
 
